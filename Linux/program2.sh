#for i in {1..10} 
#do
#    touch file$i.txt
#done

for FILE in $(find ./ -maxdepth 0 -type f -mmin +5) 
do
    #echo $FILE
    if [[ $FILE != ./program2.sh ]]
    then
        echo $FILE
        mv -v $FILE Old
    fi
done

#mv -v $(find ./ -type f +%m +5) Old